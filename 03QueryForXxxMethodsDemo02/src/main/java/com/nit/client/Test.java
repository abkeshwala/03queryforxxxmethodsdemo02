package com.nit.client;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.nit.bean.Address;
import com.nit.dao.StudentDAO;

public class Test {
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("com/nit/myBeans.xml");
		
		StudentDAO studentDao = (StudentDAO)context.getBean("studentDao");
		
		System.out.println("Testing getAddressId(int sid)...");
		System.out.println(studentDao.getAddressId(1001));
		System.out.println("=================================");
		
		System.out.println("Testing getStudentId(String name)...");
		System.out.println(studentDao.getStudentId("Ramesh"));
		System.out.println("=================================");
		
		System.out.println("Testing getStudentAddress(int sid)...");
		Address address = studentDao.getStudentAddress(1003);
		System.out.println("City : "+address.getCity());
		System.out.println("=================================");
		
		System.out.println("Testing getStudentByName(String name)...");
		Map<String,Object> map = studentDao.getStudentByName("Ramesh");
		Set<Entry<String,Object>> set = map.entrySet();
		for(Entry<String,Object> entry : set) {
			System.out.println(entry.getKey()+" : "+entry.getValue());
		}
		System.out.println("=================================");
		
		System.out.println("Testing getAllStudentDetails()...");
		List<Map<String,Object>> list = studentDao.getAllStudentDetails();
		for(Map<String,Object> map1 : list) {
			Set<Entry<String,Object>> set1 = map1.entrySet();
			for(Entry<String,Object> entry : set1) {
				System.out.println(entry.getKey()+" : "+entry.getValue());
			}
			System.out.println("--------------------------------------");
		}
	}

}
