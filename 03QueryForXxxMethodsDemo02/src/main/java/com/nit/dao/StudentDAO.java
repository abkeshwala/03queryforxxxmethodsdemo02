package com.nit.dao;

import java.util.List;
import java.util.Map;

import com.nit.bean.Address;

public interface StudentDAO {
	
	public int getAddressId(int sid);
	public long getStudentId(String name);
	public Address getStudentAddress(int sid);
	public Map<String,Object> getStudentByName(String name);
	public List<Map<String,Object>> getAllStudentDetails();

}
