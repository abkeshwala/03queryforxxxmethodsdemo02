package com.nit.dao;

import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.nit.bean.Address;
import com.nit.bean.Student;

@Repository("studentDao")
public class StudentDaoImpl implements StudentDAO {

	private JdbcTemplate jdbcTemplate;
	@Autowired
	public void setJdbcTemplate(DataSource dataSource) {
		jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	public int getAddressId(int sid) {
		int addressId = 0;
		String sql = "select addrid from student where sid="+sid;
		addressId = jdbcTemplate.queryForInt(sql);
		return addressId;
	}

	public long getStudentId(String name) {
		int studentId = 0;
		String sql = "select sid from student where name=?";
		studentId = jdbcTemplate.queryForInt(sql, name);
		return studentId;
	}

	public Address getStudentAddress(int sid) {
		Address address = new Address();
		String sql = "select city,state from student s, address a where s.addrid=a.addrid and s.sid=?";
		Map<String,Object> map = jdbcTemplate.queryForMap(sql, sid);
		Set<String> keys = map.keySet();
		/*for(String key : keys) {
			String value = (String)map.get(key);
			address.setCity(value);			
		}*/
		address.setCity((String)map.get("CITY"));		//Hardcoded value, as we know here that map object key names 
		address.setState((String)map.get("STATE"));		// are "CITY" and "STATE.
		
		return address;
	}

	public Map<String, Object> getStudentByName(String name) {
		String sql = "select * from student s, address a where s.addrid=a.addrid and s.name=?";
		Map<String,Object> map = jdbcTemplate.queryForMap(sql, name);
		return map;
	}

	public List<Map<String, Object>> getAllStudentDetails() {
		String sql = "select * from student s, address a where s.addrid=a.addrid";
		List<Map<String,Object>> list = jdbcTemplate.queryForList(sql);
		return list;
	}

}
